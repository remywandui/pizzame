const branches = [
      {"id": 8,"name": "Kovacek Plain","location": "163.8408","contact_info": "279-696-6495","address": "287 Rick Meadow"},
      {"id": 9,"name": "Keanu Track","location": "65.7387","contact_info": "919-320-7344","address": "2695 Heaney Place"},
      {"id": 10,"name": "Mraz Isle","location": "-55.0174","contact_info": "247-342-0232","address": "04324 Georgianna Estate"},
      {"id": 11,"name": "Corkery Divide","location": "176.6176","contact_info": "406-975-8977","address": "7806 Norberto Landing"},
      {"id": 12,"name": "Madelynn Club","location": "175.7120","contact_info": "291-144-3788","address": "6815 Wiegand Summit"}
  ]

function get(url) {
        return new Promise((resolve, reject) => {
            const branchId = parseInt(url.substr('/users/'.length), 10)
            process.nextTick(
                () => branches[branchId] ? resolve(branches[branchId]): reject ({
                    error: 'Branch with ' +branchId+ ' not found.',
                })
            )
        })
}

module.exports = {
    request
}
